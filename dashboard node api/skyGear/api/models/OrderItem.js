/**
* OrderItem.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	connection: 'mySqlServer',
  	tableName: 'orderItem',
  	autoPK: false,
  	autoCreatedAt: false,
	autoUpdatedAt: false,

	attributes: {

		orderItemId: {
			type: 'INTEGER',
			primaryKey: true,
			required: true
		},

		orderId: {
			type: 'INTEGER'
		},

		invId: {
			type: 'INTEGER'
		},

		quantity: {
			type: 'INTEGER'
		},

		status: {
			type: 'STRING'
		}
	}
};


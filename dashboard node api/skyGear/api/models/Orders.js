/**
* Orders.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	connection: 'mySqlServer',
  	tableName: 'orders',
  	autoPK: false,
  	autoCreatedAt: false,
	autoUpdatedAt: false,

	attributes: {

		orderId: {
			type: 'INTEGER',
			primaryKey: true,
			required: true
		},

		customerId: {
			type: 'INTEGER'
		},

		addressId: {
			type: 'INTEGER'
		},

		status: {
			type: 'STRING'
		},

		employeeId: {
			type: 'STIRNG'
		},

		datePlaced: {
			type: 'DATE'
		},

		total: {
			type: 'INTEGER'
		}
  }
};


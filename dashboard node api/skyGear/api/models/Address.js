/**
* Address.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	connection: 'mySqlServer',
  	tableName: 'address',
  	autoPK: false,
  	autoCreatedAt: false,
	autoUpdatedAt: false,

	attributes: {

		addressId: {
			type: 'INTEGER',
			primaryKey: true,
			required: true
		},

		customerId: {
			type: 'INTEGER'
		},

		address1: {
			type: 'STRING'
		},

		address2: {
			type: 'STRING'
		},

		address3: {
			type: 'STRING'
		},
		
		postcode: {
			type: 'STRING'
		},

		city: {
			type: 'STRING'
		},

		addressType: {
			type: 'STRING'
		}
	}
};


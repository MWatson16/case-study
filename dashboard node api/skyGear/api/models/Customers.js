/**
* Customers.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	connection: 'mySqlServer',
  	tableName: 'customers',
  	autoPK: false,
  	autoCreatedAt: false,
	autoUpdatedAt: false,

  	attributes: {

	  	customerId: {
	  		type: 'INTEGER',
	  		primaryKey: true,
	  		required: true
	  	},

	  	firstName: {
	  		type: 'STRING'
	  	},

	  	lastName: {
	  		type: 'STRING'
	  	},

	  	otherNames: {
	  		type: 'STRING'
	  	},

	  	DOB: {
	  		type: 'DATE'
	  	},

	  	email: {
	  		type: 'STRING'
	  	},

	  	username: {
	  		type: 'STRING'
	  	},

	  	password: {
	  		type: 'STRING'
	  	},

	  	phone: {
	  		type: 'STRING'
	  	},

	  	salt: {
	  		type: 'STRING'
	  	},

	  	customerType: {
	  		type: 'STRING'
	  	}
  	}
};


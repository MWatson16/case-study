/**
* Inventory.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	connection: 'mySqlServer',
  	tableName: 'inventory',
  	autoPK: false,
  	autoCreatedAt: false,
	autoUpdatedAt: false,

	attributes: {

		invId: {
			type: 'INTEGER',
			primaryKey: true,
			required: true
		},

		invName: {
			type: 'STRING'
		},

		category: {
			type: 'STRING'
		},

		quantity: {
			type: 'INTEGER'
		},

		price: {
			type: 'STRING'
		},

		description: {
			type: 'STRING'
		},

		size: {
			type: 'STRING'
		},

		location: {
			type: 'STRING'
		},

		reorderLevel: {
			type: 'INTEGER'
		},

		pic: {
			type: 'STRING'
		},

		reorder: {
			type: 'INTEGER'
		},
		
		reorderAmount: {
			type: 'INTEGER'
		}
	}
};


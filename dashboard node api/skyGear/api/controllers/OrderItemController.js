/**
 * OrderItemController
 *
 * @description :: Server-side logic for managing orderitems
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var OrderItemController = {
	
	getItemById: function(req, res) {

		var itemId = req.param("id");

		OrderItem.find({ where: {orderItemId: itemId }, select: ['invId', 'quantity', 'status']}).exec(function(err, item) {

			if(err) return err;

			var obj = item[0];

			console.log(obj.quantity);

			// console.log(result);
			res.json(item);
		});
	}
};
module.exports = OrderItemController;

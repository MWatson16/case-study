<?php
	error_reporting(E_ALL);
	$customer = new custClass();  
    if($_POST['login']){  
        $email = $_POST['email']; 
        $password = $_POST['password'];  
        $user = $customer->Login($email, $password);  

        if ($user) {  
            // Registration Success
           echo("<script>window.location.assign(\"home.php\")</script>");
        } else {  
            // Registration Failed  
            echo "<script>alert('email / Password Not Match')</script>";
        }  
    }  

    if($_POST['register']){  

        $username = $_POST['username'];  
        $email = $_POST['email'];  
        $password = $_POST['password'];  
        $confirmPassword = $_POST['confirm_password'];  

        if($password == $confirmPassword){  
            $email = $customer->isUserExist($email);  
            if(!$email){  
                $register = $customer->UserRegister($username, $email, $password);  
                if($register){  
                     echo "<script>alert('Registration Successful')</script>";  
                }else{  
                    echo "<script>alert('Registration Not Successful')</script>";  
                }  
            } else {  
                echo "<script>alert('Email Already Exist')</script>";  
            }  
        } else {  
            echo "<script>alert('Password Not Match')</script>";  
          
        }
    }

    if($_POST['update']){
    	$username = $_POST['username'];  
        $email = $_POST['email'];  
        $password = $_POST['password'];  
        $confirmPassword = $_POST['confirm_password'];

        if($password == $confirmPassword){

        	$updt = $customer->updateCustomerDetails($username, $email, $password);

    		if (!$updt){
    			echo "<script>alert('Error updating customer')</script>";
    		} else {
    			echo "<script>alert('Customer updated')</script>";
    		}
        } else {
        	echo "<script>alert('Password Not Match')</script>";  
        }
    }
?>